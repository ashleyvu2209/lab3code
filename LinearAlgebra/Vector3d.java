package LinearAlgebra;
public class Vector3d{
private double x;
private double y;
private double z;
public Vector3d(double x, double y, double z){
this.x = x;
this.y = y;
this.z = z;
}
public double getX(){
    return this.x;
}
public double getY(){
    return this.y;
}
public double getZ(){
    return this.z;
}
public double magnitude(){
    double m = Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2);
    m = Math.sqrt(m);
    return m;
}
public double dotProduct(double a, double b, double c){
     double result = (this.x *a)+(this.y *b)+(this.z *c);
     return result;
}
public Vector3d add(Vector3d b){
    Vector3d c = new Vector3d(0,0,0);
     c.x = this.x +b.x;
     c.y= this.y+b.y;
     c.z = this.z +b.z;
    return c;
}
}