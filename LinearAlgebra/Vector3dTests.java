
//Ashley Vu - 2034284 
package LinearAlgebra;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Vector3dTests {
    Vector3d a = new Vector3d(3,4,5);
    @Test 
    public void testGetX(){
        assertEquals(3, a.getX());
    }
    @Test 
    public void testMagnitude(){
        assertEquals(7.0710678118654755, a.magnitude(), .0001);
    }
    @Test 
    public void testAdd(){
        Vector3d b = new Vector3d(7,8,9);
        Vector3d c = a.add(b);
        assertEquals(10,c.getX());
        assertEquals(12,c.getY());
        assertEquals(14,c.getZ());
    }
    @Test
    public void testDotProduct(){         
        assertEquals(26, a.dotProduct(1,2,3));
    }
}